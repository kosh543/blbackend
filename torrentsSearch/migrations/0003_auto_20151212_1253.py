# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2015-12-12 06:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('torrentsSearch', '0002_auto_20151212_1239'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='torrent',
            name='download_link',
        ),
        migrations.AlterField(
            model_name='torrent',
            name='torrent_link',
            field=models.URLField(),
        ),
    ]
