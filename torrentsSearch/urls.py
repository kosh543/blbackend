from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from torrentsSearch.views import TorrentsViewSet, index

router = DefaultRouter()
router.register(r'torrents', TorrentsViewSet)

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^api/', include(router.urls)),
]
