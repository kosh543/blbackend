from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Torrent(models.Model):
    name = models.CharField(max_length=200)
    author = models.CharField(max_length=50, default="")
    verified_author = models.BooleanField(default=False)
    category = models.CharField(max_length=50, default="")
    size = models.CharField(max_length=50, default="")
    files = models.CharField(max_length=50, default="")
    age = models.CharField(max_length=50, default="")
    seed = models.CharField(max_length=50, default="")
    leech = models.CharField(max_length=50, default="")
    verified_torrent = models.BooleanField(default=False)
    comments = models.CharField(max_length=200, default="")
    torrent_link = models.URLField(default="")
    magnet_link = models.URLField(max_length=500, default="")
    download_link = models.URLField(max_length=500, default="")

    def __str__(self):
        return self.name
