from torrentsSearch.models import Torrent
from torrentsSearch.serializers import TorrentSerializer
from rest_framework import viewsets
from django.shortcuts import render


def index(request):
    latest_list = Torrent.objects.all()
    context = {'latest_list': latest_list}
    return render(request, 'torrentsSearch/index.html', context)


class TorrentsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Torrent.objects.all()
    serializer_class = TorrentSerializer
