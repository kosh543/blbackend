from torrentsSearch.models import Torrent
from rest_framework import serializers


class TorrentSerializer(serializers.HyperlinkedModelSerializer):   
    class Meta:
        model = Torrent
        fields = ('url', 'name', 'author', 'verified_author',
                  'category', 'size', 'files', 'age', 'seed',
                  'leech', 'verified_torrent', 'comments',
                  'torrent_link', 'magnet_link', 'download_link')
