from __future__ import absolute_import

from celery import task
from celery.utils.log import get_task_logger

from .models import Torrent
from .KickassAPI import Latest

logger = get_task_logger(__name__)


@task(name="add_latest_torrents")
def add_latest_torrents():
    torrents_list = Latest()
    for t in torrents_list:
        logger.info(t.name)
        if len(Torrent.objects.filter(torrent_link=t.torrent_link)) > 0:
            continue
        try:
            q = Torrent(name=t.name, author=t.author,
                        verified_author=t.verified_author,
                        category=t.category, size=t.size,
                        files=t.files, age=t.age,
                        seed=t.seed, leech=t.leech,
                        verified_torrent=t.verified_torrent,
                        comments=t.comments,
                        torrent_link=t.torrent_link,
                        magnet_link=t.magnet_link,
                        download_link=t.download_link)
            q.save()
        except:
            pass
