# -*- coding: utf8 -*-

from django.test import TestCase

import mock

from .tasks import add_latest_torrents
from .models import Torrent


class AddLatestTorrentsTests(TestCase):

    @mock.patch('requests.get')
    def test_successful_call(self, mock_get):
        f = open('torrentsSearch/successful_call.txt', 'r')
        mock_response = mock.Mock()
        mock_response.text = f.read()
        f.close()
        mock_get.return_value = mock_response
        add_latest_torrents()
        self.assertEqual(len(Torrent.objects.all()), 25)
