from django.apps import AppConfig


class TorrentssearchConfig(AppConfig):
    name = 'torrentsSearch'
